# Ninox Pilot Logbook

Please note that a pre-requisite of using this logbook is that you have Ninox database on your Mac or iDevice.

The logbook is reasonably simple and is designed for the private pilot but is easily extensible.

I wrote this because I didn't like the Excel or cloud options around. I hope it's useful to you.

## Structure

The database has 3 tables:

- Pilot - this is where you enter a name for each pilot in the logbook. For your name enter "Self". For everyone else you should probably enter as an initial and family name: J Smith. this is in order to ensure the column isn't too wide in the resulting logbook

- Aircraft - make an entry for each aircraft in your logbook. Entries should be Description: Cessna 172E; Type: the official ICAO designator so C172; Rego: the Aircraft registration - ZK-OMR

- Logbook - This is where you make the entries for your flying time.

## Use

1. Make an entry or two in the Pilots table

1. Make an entry or two in the Aircraft table

1. Create a new entry in the Logbook
	
	1. Click on the Logbook tab
	
	1. Create a new record
	
	1. In the date field enter the date as "14 Dec 1903" and Ninox deals with it
	
	1. Click in Aircraft field and choose your aircraft
	
	1. In the PIC field click and choose a pilot
	
	1. Do the same in the "Other Pilot" if there is one
	
	1. Enter the details of the flight
	
	1. Select Dual or Solo
	
	1. Select Day/Night
	
	1. Select XCountry or non-XCountry
	
	1. Enter the hours in decimal hours
	
	1. If there is instrument time enter the portion of the time that was instrument in decimal hours
	
	1. Enter the number of landings
	
## Reporting

### 90 Day currency

This view will tell you the total number of hours and landings in the last 90 days - look at the bottom of the page for totals

### Hours by Type

This view shows you a chart of the hours by type. You can change the chart types using the menu on the right.

### Summary

this view gives a cross tab that shows hours broken down by XCountry or not and by Dual, Solo, Instrument and Total

